-- import GIS extension
CREATE EXTENSION IF NOT EXISTS postgis;

-- SETUP proper zone encoding
SELECT AddGeometryColumn('TaxiZones', 'geom_4326', 4326, 'MULTIPOLYGON', 2);
UPDATE "TaxiZones"
SET "geom_4326" = ST_Transform(geom, 4326);
ALTER TABLE "TaxiZones"
DROP COLUMN geom;
ALTER TABLE "TaxiZones"
RENAME COLUMN geom_4326 to geom;
CREATE INDEX taxi_zones_geom_idx ON "TaxiZones" USING GiST (geom);

-- Drop redundent IDs
ALTER TABLE "TaxiZones"
DROP COLUMN objectid,
DROP COLUMN locationid;

-- Business Records Neighbourhoods

CREATE INDEX idx_taxizones_geom ON "TaxiZones" USING GIST (geom);

ALTER TABLE "BusinessRecords"
ADD COLUMN business_zone INT;

UPDATE "BusinessRecords" br
SET business_zone = tz.gid
FROM "TaxiZones" tz
WHERE ST_Contains(tz.geom, ST_SetSRID(ST_MakePoint(br.longitude, br.latitude), 4326));

CREATE INDEX idx_business_zones ON "BusinessRecords"(business_zone);

-- Restaurant Records Neighbourhoods

ALTER TABLE "RestaurantRecords"
ADD COLUMN restaurant_zone INT;

UPDATE "RestaurantRecords" rr
SET restaurant_zone = tz.gid
FROM "TaxiZones" tz
WHERE ST_Contains(tz.geom, ST_SetSRID(ST_MakePoint(rr.longitude, rr.latitude), 4326));

CREATE INDEX idx_restaurant_zones ON "RestaurantRecords"(restaurant_zone);



-- Arrest Records Neighbourhoods

ALTER TABLE "ArrestRecords"
ADD COLUMN arrest_zone INT;

UPDATE "ArrestRecords" ar
SET arrest_zone = tz.gid
FROM "TaxiZones" tz
WHERE ST_Contains(tz.geom, ST_SetSRID(ST_MakePoint(ar.longitude, ar.latitude), 4326));

CREATE INDEX idx_arrest_zones ON "ArrestRecords"(arrest_zone);


-- Borough Crash Neighbourhoods

ALTER TABLE "BoroughCrashes"
ADD COLUMN borough_crash_zone INT;

UPDATE "BoroughCrashes" bc
SET borough_crash_zone = tz.gid
FROM "TaxiZones" tz
WHERE ST_Contains(tz.geom, ST_SetSRID(ST_MakePoint(bc.longitude, bc.latitude), 4326));

CREATE INDEX idx_crash_zones ON "BoroughCrashes"(borough_crash_zone);


-- Property Neighbourhoods
ALTER TABLE "PropertyRecords"
ADD COLUMN property_zone INT;

UPDATE "PropertyRecords" pr
SET property_zone = tz.gid
FROM "TaxiZones" tz
WHERE ST_Contains(tz.geom, ST_SetSRID(ST_MakePoint(pr.longitude, pr.latitude), 4326));

CREATE INDEX idx_property_zones ON "PropertyRecords"(property_zone);


