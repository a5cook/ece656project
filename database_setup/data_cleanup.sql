-- Business Record Cleanup
ALTER TABLE "BusinessRecords"
DROP COLUMN "Category Name",
DROP COLUMN "URL",
DROP COLUMN "Hiring",
DROP COLUMN "Why NYC",
DROP COLUMN "Jobs URL";

ALTER TABLE "BusinessRecords" RENAME COLUMN "Postcode" TO zipcode;
ALTER TABLE "BusinessRecords" RENAME COLUMN "BBL" TO bbl;
ALTER TABLE "BusinessRecords" RENAME COLUMN "BIN" TO bin;
ALTER TABLE "BusinessRecords" RENAME COLUMN "Census Tract" TO census_tract;
ALTER TABLE "BusinessRecords" RENAME COLUMN "Council District" TO council_district;
ALTER TABLE "BusinessRecords" RENAME COLUMN "Community Board" TO community_board;
ALTER TABLE "BusinessRecords" RENAME COLUMN "Longitude" TO longitude;
ALTER TABLE "BusinessRecords" RENAME COLUMN "Latitude" TO latitude;
ALTER TABLE "BusinessRecords" RENAME COLUMN "Address" TO address;
ALTER TABLE "BusinessRecords" RENAME COLUMN "Company Name" TO company_name;
ALTER TABLE "BusinessRecords" RENAME COLUMN "borough" TO business_borough;


ALTER TABLE "BusinessRecords" ALTER COLUMN zipcode TYPE INT USING zipcode::INT;
ALTER TABLE "BusinessRecords" ALTER COLUMN community_board TYPE INT;
ALTER TABLE "BusinessRecords" ALTER COLUMN council_district TYPE INT;
ALTER TABLE "BusinessRecords" ALTER COLUMN census_tract TYPE INT;
ALTER TABLE "BusinessRecords" ALTER COLUMN bin TYPE INT;
ALTER TABLE "BusinessRecords" ALTER COLUMN bbl TYPE BIGINT;

ALTER TABLE "BusinessRecords" ALTER COLUMN latitude SET NOT NULL;
ALTER TABLE "BusinessRecords" ALTER COLUMN longitude SET NOT NULL;

-- Restaurant Records Cleanup
ALTER TABLE "RestaurantRecords"
DROP COLUMN "IsRoadwayCompliant",
DROP COLUMN "IsSidewayCompliant",
DROP COLUMN "SkippedReason",
DROP COLUMN "AgencyCode",
DROP COLUMN "NTA";

DELETE FROM "RestaurantRecords"
WHERE "RestaurantRecords".restaurant_name IS NULL;

ALTER TABLE "RestaurantRecords" RENAME COLUMN "Borough" TO restaurant_borough;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "RestaurantName" TO restaurant_name;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "SeatingChoice" TO seating_choice;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "BusinessAddress" TO address;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "RestaurantInspectionID" TO restaurant_inspection_id;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "SeatingChoice" TO seating_choice;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "Postcode" TO zipcode;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "Longitude" TO longitude;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "Latitude" TO latitude;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "CommunityBoard" TO community_board;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "CouncilDistrict" TO council_district;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "CensusTract" TO census_tract;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "BIN" TO bin;
ALTER TABLE "RestaurantRecords" RENAME COLUMN "BBL" TO bbl;

ALTER TABLE "RestaurantRecords" ALTER COLUMN zipcode TYPE INT USING zipcode::INT;
ALTER TABLE "RestaurantRecords" ALTER COLUMN zipcode TYPE INT USING zipcode::INT;
ALTER TABLE "RestaurantRecords" ALTER COLUMN community_board TYPE INT;
ALTER TABLE "RestaurantRecords" ALTER COLUMN council_district TYPE INT;
ALTER TABLE "RestaurantRecords" ALTER COLUMN census_tract TYPE INT;
ALTER TABLE "RestaurantRecords" ALTER COLUMN bin TYPE INT;
ALTER TABLE "RestaurantRecords" ALTER COLUMN bbl TYPE BIGINT;
UPDATE "RestaurantRecords"
SET inspected_on = TO_TIMESTAMP(inspected_on, 'MM/DD/YYYY HH12:MI:SS AM')::DATE;
ALTER TABLE "RestaurantRecords" ALTER COLUMN inspected_on TYPE DATE USING inspected_on::DATE;
WITH MostRecent AS (
    SELECT restaurant_name, address, MAX(inspected_on) AS MaxInspectedOn
    FROM "RestaurantRecords"
    GROUP BY restaurant_name, address
)
DELETE FROM "RestaurantRecords"
WHERE (restaurant_name, address, inspected_on) NOT IN (
    SELECT restaurant_name, address, MaxInspectedOn
    FROM MostRecent
);



--ArrestRecord Cleanup
ALTER TABLE "ArrestRecords" RENAME COLUMN "ARREST_KEY" TO "arrest_key";
ALTER TABLE "ArrestRecords" RENAME COLUMN "ARREST_DATE" TO "arrest_date";
ALTER TABLE "ArrestRecords" RENAME COLUMN "PD_CD" TO "pd_cd";
ALTER TABLE "ArrestRecords" RENAME COLUMN "PD_DESC" TO "pd_desc";
ALTER TABLE "ArrestRecords" RENAME COLUMN "KY_CD" TO "ky_cd";
ALTER TABLE "ArrestRecords" RENAME COLUMN "OFNS_DESC" TO "ofns_desc";
ALTER TABLE "ArrestRecords" RENAME COLUMN "LAW_CODE" TO "law_code";
ALTER TABLE "ArrestRecords" RENAME COLUMN "LAW_CAT_CD" TO "law_cat_cd";
ALTER TABLE "ArrestRecords" RENAME COLUMN "ARREST_BORO" TO "arrest_borough";
ALTER TABLE "ArrestRecords" RENAME COLUMN "ARREST_PRECINCT" TO "arrest_precinct";
ALTER TABLE "ArrestRecords" RENAME COLUMN "JURISDICTION_CODE" TO "jurisdiction_code";
ALTER TABLE "ArrestRecords" RENAME COLUMN "AGE_GROUP" TO "age_group";
ALTER TABLE "ArrestRecords" RENAME COLUMN "PERP_SEX" TO "perp_sex";
ALTER TABLE "ArrestRecords" RENAME COLUMN "PERP_RACE" TO "perp_race";
ALTER TABLE "ArrestRecords" RENAME COLUMN "X_COORD_CD" TO "x_coord_cd";
ALTER TABLE "ArrestRecords" RENAME COLUMN "Y_COORD_CD" TO "y_coord_cd";
ALTER TABLE "ArrestRecords" RENAME COLUMN "Latitude" TO "latitude";
ALTER TABLE "ArrestRecords" RENAME COLUMN "Longitude" TO "longitude";
ALTER TABLE "ArrestRecords" RENAME COLUMN "New Georeferenced Column" TO "new_georeferenced_column";

CREATE TYPE age_group_enum AS ENUM ('18-24', '25-44', '45-64', '65+', '<18');

ALTER TABLE "ArrestRecords" ALTER COLUMN arrest_key TYPE BIGINT;
ALTER TABLE "ArrestRecords" ALTER COLUMN pd_cd TYPE SMALLINT;
ALTER TABLE "ArrestRecords" ALTER COLUMN ky_cd TYPE SMALLINT;
ALTER TABLE "ArrestRecords" ALTER COLUMN arrest_precinct TYPE SMALLINT;
ALTER TABLE "ArrestRecords" ALTER COLUMN jurisdiction_code TYPE SMALLINT;
ALTER TABLE "ArrestRecords" ALTER COLUMN x_coord TYPE INTEGER;
ALTER TABLE "ArrestRecords" ALTER COLUMN y_coord TYPE INTEGER;
ALTER TABLE "ArrestRecords" ALTER COLUMN arrest_date TYPE DATE USING arrest_date::DATE;
ALTER TABLE "ArrestRecords" ALTER COLUMN latitude TYPE DOUBLE PRECISION;
ALTER TABLE "ArrestRecords" ALTER COLUMN longitude TYPE DOUBLE PRECISION;
ALTER TABLE "ArrestRecords" ALTER COLUMN perp_sex TYPE CHAR;
ALTER TABLE "ArrestRecords" ALTER COLUMN perp_race TYPE VARCHAR(30);
ALTER TABLE "ArrestRecords" ALTER COLUMN age_group TYPE age_group_enum USING age_group::age_group_enum;

ALTER TABLE "ArrestRecords" ALTER COLUMN latitude SET NOT NULL;
ALTER TABLE "ArrestRecords" ALTER COLUMN longitude SET NOT NULL;

-- Crash Record Cleanup
ALTER TABLE "CrashRecords" DROP COLUMN "LOCATION";

ALTER TABLE "CrashRecords" RENAME COLUMN "CRASH DATE" TO "crash_date";
ALTER TABLE "CrashRecords" RENAME COLUMN "CRASH TIME" TO "crash_time";
ALTER TABLE "CrashRecords" RENAME COLUMN "BOROUGH" TO "crash_borough";
ALTER TABLE "CrashRecords" RENAME COLUMN "ZIP CODE" TO "zipcode";
ALTER TABLE "CrashRecords" RENAME COLUMN "Latitude" TO "latitude";
ALTER TABLE "CrashRecords" RENAME COLUMN "Longitude" TO "longitude";
ALTER TABLE "CrashRecords" RENAME COLUMN "ON STREET NAME" TO "on_street_name";
ALTER TABLE "CrashRecords" RENAME COLUMN "CROSS STREET NAME" TO "cross_street_name";
ALTER TABLE "CrashRecords" RENAME COLUMN "OFF STREET NAME" TO "off_street_name";
ALTER TABLE "CrashRecords" RENAME COLUMN "NUMBER OF PERSONS INJURED" TO "number_of_persons_injured";
ALTER TABLE "CrashRecords" RENAME COLUMN "NUMBER OF PERSONS KILLED" TO "number_of_persons_killed";
ALTER TABLE "CrashRecords" RENAME COLUMN "NUMBER OF PEDESTRIANS INJURED" TO "number_of_pedestrians_injured";
ALTER TABLE "CrashRecords" RENAME COLUMN "NUMBER OF PEDESTRIANS KILLED" TO "number_of_pedestrians_killed";
ALTER TABLE "CrashRecords" RENAME COLUMN "NUMBER OF CYCLIST INJURED" TO "number_of_cyclist_injured";
ALTER TABLE "CrashRecords" RENAME COLUMN "NUMBER OF CYCLIST KILLED" TO "number_of_cyclist_killed";
ALTER TABLE "CrashRecords" RENAME COLUMN "NUMBER OF MOTORIST INJURED" TO "number_of_motorist_injured";
ALTER TABLE "CrashRecords" RENAME COLUMN "NUMBER OF MOTORIST KILLED" TO "number_of_motorist_killed";
ALTER TABLE "CrashRecords" RENAME COLUMN "CONTRIBUTING FACTOR VEHICLE 1" TO "contributing_factor_vehicle_1";
ALTER TABLE "CrashRecords" RENAME COLUMN "CONTRIBUTING FACTOR VEHICLE 2" TO "contributing_factor_vehicle_2";
ALTER TABLE "CrashRecords" RENAME COLUMN "CONTRIBUTING FACTOR VEHICLE 3" TO "contributing_factor_vehicle_3";
ALTER TABLE "CrashRecords" RENAME COLUMN "CONTRIBUTING FACTOR VEHICLE 4" TO "contributing_factor_vehicle_4";
ALTER TABLE "CrashRecords" RENAME COLUMN "CONTRIBUTING FACTOR VEHICLE 5" TO "contributing_factor_vehicle_5";
ALTER TABLE "CrashRecords" RENAME COLUMN "COLLISION_ID" TO "collision_id";
ALTER TABLE "CrashRecords" RENAME COLUMN "VEHICLE TYPE CODE 1" TO "vehicle_type_code_1";
ALTER TABLE "CrashRecords" RENAME COLUMN "VEHICLE TYPE CODE 2" TO "vehicle_type_code_2";
ALTER TABLE "CrashRecords" RENAME COLUMN "VEHICLE TYPE CODE 3" TO "vehicle_type_code_3";
ALTER TABLE "CrashRecords" RENAME COLUMN "VEHICLE TYPE CODE 4" TO "vehicle_type_code_4";
ALTER TABLE "CrashRecords" RENAME COLUMN "VEHICLE TYPE CODE 5" TO "vehicle_type_code_5";

ALTER TABLE "CrashRecords" ALTER COLUMN number_of_persons_injured TYPE INT;
ALTER TABLE "CrashRecords" ALTER COLUMN number_of_persons_killed TYPE INT;
ALTER TABLE "CrashRecords" ALTER COLUMN number_of_pedestrians_injured TYPE INT;
ALTER TABLE "CrashRecords" ALTER COLUMN number_of_pedestrians_killed TYPE INT;
ALTER TABLE "CrashRecords" ALTER COLUMN number_of_cyclist_injured TYPE INT;
ALTER TABLE "CrashRecords" ALTER COLUMN number_of_cyclist_killed TYPE INT;
ALTER TABLE "CrashRecords" ALTER COLUMN number_of_motorist_injured TYPE INT;
ALTER TABLE "CrashRecords" ALTER COLUMN number_of_motorist_killed TYPE INT;
ALTER TABLE "CrashRecords" ALTER COLUMN zipcode TYPE INT USING zipcode::INT;
ALTER TABLE "CrashRecords" ALTER COLUMN crash_date TYPE DATE USING crash_date::DATE;
ALTER TABLE "CrashRecords" ALTER COLUMN crash_time TYPE TIME USING crash_time::TIME;

ALTER TABLE "CrashRecords" ALTER COLUMN latitude SET NOT NULL;
ALTER TABLE "CrashRecords" ALTER COLUMN longitude SET NOT NULL;


-- Property Records
-- Only interested in residential and small business so taxclass 1 only

DELETE FROM "PropertyRecords" WHERE "TAXCLASS" != '1';

ALTER TABLE "PropertyRecords" DROP COLUMN "BORO";


ALTER TABLE "PropertyRecords" RENAME COLUMN "BBLE" TO bble;
ALTER TABLE "PropertyRecords" RENAME COLUMN "YEAR" TO year;
ALTER TABLE "PropertyRecords" RENAME COLUMN "BLOCK" TO block;
ALTER TABLE "PropertyRecords" RENAME COLUMN "LOT" TO lot;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EASEMENT" TO easement;
ALTER TABLE "PropertyRecords" RENAME COLUMN "OWNER" TO owner;
ALTER TABLE "PropertyRecords" RENAME COLUMN "BLDGCL" TO bldgcl;
ALTER TABLE "PropertyRecords" RENAME COLUMN "TAXCLASS" TO taxclass;
ALTER TABLE "PropertyRecords" RENAME COLUMN "LTFRONT" TO ltfront;
ALTER TABLE "PropertyRecords" RENAME COLUMN "LTDEPTH" TO ltdepth;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EXT" TO ext;
ALTER TABLE "PropertyRecords" RENAME COLUMN "STORIES" TO stories;
ALTER TABLE "PropertyRecords" RENAME COLUMN "FULLVAL" TO fullval;
ALTER TABLE "PropertyRecords" RENAME COLUMN "AVLAND" TO avland;
ALTER TABLE "PropertyRecords" RENAME COLUMN "AVTOT" TO avtot;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EXLAND" TO exland;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EXTOT" TO extot;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EXCD1" TO excd1;
ALTER TABLE "PropertyRecords" RENAME COLUMN "STADDR" TO staddr;
ALTER TABLE "PropertyRecords" RENAME COLUMN "POSTCODE" TO postcode;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EXMPTCL" TO exmptcl;
ALTER TABLE "PropertyRecords" RENAME COLUMN "BLDFRONT" TO bldfront;
ALTER TABLE "PropertyRecords" RENAME COLUMN "BLDDEPTH" TO blddepth;
ALTER TABLE "PropertyRecords" RENAME COLUMN "AVLAND2" TO avland2;
ALTER TABLE "PropertyRecords" RENAME COLUMN "AVTOT2" TO avtot2;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EXLAND2" TO exland2;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EXTOT2" TO extot2;
ALTER TABLE "PropertyRecords" RENAME COLUMN "EXCD2" TO excd2;
ALTER TABLE "PropertyRecords" RENAME COLUMN "PERIOD" TO period;
ALTER TABLE "PropertyRecords" RENAME COLUMN "VALTYPE" TO valtype;
ALTER TABLE "PropertyRecords" RENAME COLUMN "Borough" TO property_borough;
ALTER TABLE "PropertyRecords" RENAME COLUMN "Latitude" TO latitude;
ALTER TABLE "PropertyRecords" RENAME COLUMN "Longitude" TO longitude;
ALTER TABLE "PropertyRecords" RENAME COLUMN "Community Board" TO community_board;
ALTER TABLE "PropertyRecords" RENAME COLUMN "Council District" TO council_district;
ALTER TABLE "PropertyRecords" RENAME COLUMN "Census Tract" TO census_tract;
ALTER TABLE "PropertyRecords" RENAME COLUMN "BIN" TO bin;
ALTER TABLE "PropertyRecords" RENAME COLUMN "NTA" TO nta;


-- TaxiRecrods

ALTER TABLE "TaxiRecords" ALTER COLUMN "RatecodeID" TYPE INT;

