
-- T1
SELECT
    zone,
    ST_Distance(
        geom::geography,
        ST_SetSRID(ST_Point(-73.985130, 40.758896), 4326)::geography
    ) AS distance_meters
FROM
    "TaxiZones"
ORDER BY
    distance_meters
LIMIT 1;

-- T2 Test case
INSERT INTO "CrashRecords" (
    number_of_persons_injured,
    number_of_pedestrians_injured,
    number_of_cyclist_injured,
    number_of_motorist_injured,
    number_of_persons_killed,
    number_of_pedestrians_killed,
    number_of_cyclist_killed,
    number_of_motorist_killed
) VALUES (
    5, -- Total persons injured
    2, -- Pedestrians injured
    2, -- Cyclists injured
    1, -- Motorists injured
    2, -- Total persons killed
    1, -- Pedestrians killed
    0, -- Cyclists killed
    1  -- Motorists killed
);

-- T3 Test Case

INSERT INTO "CrashRecords" (
    crash_date,
    crash_time,
    zipcode,
    latitude,
    longitude,
    on_street_name,
    cross_street_name,
    off_street_name,
    number_of_persons_injured,
    number_of_persons_killed,
    number_of_pedestrians_injured,
    number_of_pedestrians_killed,
    number_of_cyclist_injured,
    number_of_cyclist_killed,
    number_of_motorist_injured,
    number_of_motorist_killed,
    contributing_factor_vehicle_1,
    contributing_factor_vehicle_2,
    contributing_factor_vehicle_3,
    contributing_factor_vehicle_4,
    contributing_factor_vehicle_5,
    collision_id,
    vehicle_type_code_1,
    vehicle_type_code_2,
    vehicle_type_code_3,
    vehicle_type_code_4,
    vehicle_type_code_5
) VALUES (
    '2024-04-19', -- Example crash_date
    '13:30:00',   -- Example crash_time
    '10001',      -- Example zipcode
    NULL,      -- Example latitude
    NULL,     -- Example longitude
    'Broadway',   -- Example on_street_name
    '5th Avenue', -- Example cross_street_name
    NULL,         -- Example off_street_name
    2,            -- Example number_of_persons_injured
    0,            -- Example number_of_persons_killed
    1,            -- Example number_of_pedestrians_injured
    0,            -- Example number_of_pedestrians_killed
    0,            -- Example number_of_cyclist_injured
    0,            -- Example number_of_cyclist_killed
    1,            -- Example number_of_motorist_injured
    0,            -- Example number_of_motorist_killed
    'Driver Inattention/Distraction', -- Example contributing_factor_vehicle_1
    NULL,         -- Example contributing_factor_vehicle_2
    NULL,         -- Example contributing_factor_vehicle_3
    NULL,         -- Example contributing_factor_vehicle_4
    NULL,         -- Example contributing_factor_vehicle_5
    456789,       -- Example collision_id
    'PASSENGER VEHICLE', -- Example vehicle_type_code_1
    'BICYCLE',           -- Example vehicle_type_code_2
    NULL,                -- Example vehicle_type_code_3
    NULL,                -- Example vehicle_type_code_4
    NULL                 -- Example vehicle_type_code_5
);

-- T4 Test Case

INSERT INTO "Borough"(name)
VALUES ('ON');

-- T5 Test Case
INSERT INTO "TaxiRecords" ("VendorID", tpep_pickup_datetime,
                         tpep_dropoff_datetime, passenger_count, trip_distance,
                         "RatecodeID", store_and_fwd_flag, "PULocationID", "DOLocationID",
                         payment_type, fare_amount, extra, mta_tax, tip_amount, tolls_amount,
                         improvement_surcharge, total_amount, congestion_surcharge, "Airport_fee",
                         "TripId")
VALUES
(1234, '2024-04-18 08:00:00',
 '2024-04-18 08:30:00', 2, 5.6, 1, 'N', 1, 2, 1, 25.5, 1.5,
 0.5, 5.0, 0.0, 0.3, 33.8, 0.0, 2.0, 1);


-- T6 Test Case
SELECT
    sum(ST_Area(geom::geography)) AS total_area_m2
FROM
    "TaxiZones"
WHERE
    borough = 'MN';

-- T7 Test Case

INSERT INTO "RestaurantRecords"
    (restaurant_name, seating_choice, address, restaurant_inspection_id,
     inspected_on, zipcode, latitude, longitude, community_board, council_district, census_tract,
     bin, bbl, restaurant_borough, restaurant_zone)
VALUES ('Sample Restaurant', 'Indoor', '123 Sample St', '123456789', '2024-04-20', '10001',
        40.1234, -73.5678, 12, 45, 67, 89, 10,
        'ON', 'Zone');

-- T8 Test Case

SELECT COUNT(name) AS name_count
FROM "Borough";

-- T10 test case
SELECT name FROM "Borough"
where name = 'MN';

-- T11 Test case

WITH ZonePairs AS (
    SELECT
        a.zone AS zone1,
        b.zone AS zone2,
        ST_Distance(a.geom::geography, b.geom::geography) AS distance
    FROM
        "TaxiZones" a
    CROSS JOIN
        "TaxiZones" b
)
SELECT
    zone1,
    zone2,
    distance
FROM
    ZonePairs
ORDER BY
    distance DESC
LIMIT 1;

