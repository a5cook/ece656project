--Business Records Keys
ALTER TABLE "BusinessRecords"
ADD CONSTRAINT fk_borough
FOREIGN KEY (business_borough) REFERENCES "Borough"(name);
ALTER TABLE "BusinessRecords"
ADD CONSTRAINT pk_businessrecords PRIMARY KEY (company_name, address);

--Arrest Records Keys
ALTER TABLE "ArrestRecords"
ADD CONSTRAINT fk_borough
FOREIGN KEY (arrest_borough) REFERENCES "Borough"(name);
ALTER TABLE "ArrestRecords"
ADD CONSTRAINT pk_arrestrecords PRIMARY KEY (arrest_key);




--Restaurant Records Keys
-- setup restaurant records as child of business records
INSERT INTO "BusinessRecords" (company_name, address, zipcode, latitude, longitude,
                               community_board, council_district, census_tract,
                               bin, bbl, business_borough)
SELECT rr.restaurant_name, rr.address, rr.zipcode, rr.latitude, rr.longitude,
       rr.community_board, rr.council_district, rr.census_tract,
       rr.bin, rr.bbl, rr.restaurant_borough
FROM "RestaurantRecords" rr
WHERE NOT EXISTS (
    SELECT 1
    FROM "BusinessRecords" br
    WHERE br.company_name = rr.restaurant_name
);


ALTER TABLE "RestaurantRecords"
ADD CONSTRAINT pk_restaurant PRIMARY KEY (restaurant_name, address);
-- restaurant name and address foreign to business records
ALTER TABLE "RestaurantRecords"
ADD CONSTRAINT fk_restaurant_business
FOREIGN KEY (restaurant_name, address)
REFERENCES "BusinessRecords"(company_name, address);

-- restaurant borough key setup
ALTER TABLE "RestaurantRecords"
ADD CONSTRAINT fk_restaurant_borough
FOREIGN KEY (restaurant_borough) REFERENCES "Borough"(name);

-- Crash Records Keys
ALTER TABLE "CrashRecords"
ADD CONSTRAINT pk_crashrecords PRIMARY KEY (collision_id);

-- Borough Crashes Keys
ALTER TABLE "BoroughCrashes"
ADD CONSTRAINT pk_boroughcrashrecords PRIMARY KEY (collision_id);
ALTER TABLE "BoroughCrashes"
ADD CONSTRAINT fk_borough
FOREIGN KEY (crash_borough) REFERENCES "Borough"(name);


-- Property Records Key
ALTER TABLE "PropertyRecords"
ADD CONSTRAINT pk_propertyrecords PRIMARY KEY (bble, year);
ALTER TABLE "PropertyRecords"
ADD CONSTRAINT fk_property_borough
FOREIGN KEY (property_borough) REFERENCES "Borough"(name);

-- Taxi Records KEY

ALTER TABLE "TaxiRecords"
ADD COLUMN "TripID" SERIAL PRIMARY KEY;

-- TaxiZones key

ALTER TABLE "TaxiZones"
ADD CONSTRAINT fk_zone_borough
FOREIGN KEY (borough) REFERENCES "Borough"(name);

CREATE INDEX idx_arrest_date ON "ArrestRecords"(arrest_date);







