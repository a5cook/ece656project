-- borough table setup
CREATE TYPE borough_name AS ENUM ('MN', 'BK', 'QN', 'BX', 'SI', 'EWR');




CREATE TABLE "Borough" (
    name borough_name PRIMARY KEY
);

INSERT INTO "Borough" (name) VALUES
('MN'),
('BK'),
('QN'),
('BX'),
('SI'),
('EWR');

-- UPDATE taxi zone borough relation
UPDATE "TaxiZones" SET borough = 'MN' WHERE TRIM("borough") = 'Manhattan';
UPDATE "TaxiZones" SET borough = 'BK' WHERE TRIM("borough") = 'Brooklyn';
UPDATE "TaxiZones" SET borough = 'QN' WHERE TRIM("borough") = 'Queens';
UPDATE "TaxiZones" SET borough = 'SI' WHERE TRIM("borough") = 'Staten Island';
UPDATE "TaxiZones" SET borough = 'BX' WHERE TRIM("borough") = 'Bronx';

ALTER TABLE "TaxiZones" ADD COLUMN temp_borough borough_name;
UPDATE "TaxiZones" SET temp_borough = "borough"::borough_name;
ALTER TABLE "TaxiZones" DROP COLUMN "borough";
ALTER TABLE "TaxiZones" RENAME COLUMN temp_borough TO borough;

-- Update BusinessRecords to conform to borough relation
UPDATE "BusinessRecords" SET business_borough = 'MN' WHERE TRIM("business_borough") = 'MANHATTAN';
UPDATE "BusinessRecords" SET "business_borough" = 'BK' WHERE TRIM("business_borough") = 'BROOKLYN';
UPDATE "BusinessRecords" SET "business_borough" = 'QN' WHERE TRIM("business_borough") = 'QUEENS';
UPDATE "BusinessRecords" SET "business_borough" = 'BX' WHERE TRIM("business_borough") = 'BRONX';
UPDATE "BusinessRecords" SET "business_borough" = 'SI' WHERE TRIM("business_borough") = 'STATEN ISLAND';

ALTER TABLE "BusinessRecords" ADD COLUMN temp_borough borough_name;
UPDATE "BusinessRecords" SET temp_borough = "business_borough"::borough_name;
ALTER TABLE "BusinessRecords" DROP COLUMN "business_borough";
ALTER TABLE "BusinessRecords" RENAME COLUMN temp_borough TO business_borough;


-- Update RestaurantRecords to conform to borough relation
UPDATE "RestaurantRecords" SET "restaurant_borough" = 'MN' WHERE TRIM("restaurant_borough") = 'Manhattan';
UPDATE "RestaurantRecords" SET "restaurant_borough" = 'BK' WHERE TRIM("restaurant_borough") = 'Brooklyn';
UPDATE "RestaurantRecords" SET "restaurant_borough" = 'QN' WHERE TRIM("restaurant_borough") = 'Queens';
UPDATE "RestaurantRecords" SET "restaurant_borough" = 'BX' WHERE TRIM("restaurant_borough") = 'Bronx';
UPDATE "RestaurantRecords" SET "restaurant_borough" = 'SI' WHERE TRIM("restaurant_borough") = 'Staten Island';

ALTER TABLE "RestaurantRecords" ADD COLUMN temp_borough borough_name;
UPDATE "RestaurantRecords" SET temp_borough = "borough"::borough_name;
ALTER TABLE "RestaurantRecords" DROP COLUMN "borough";
ALTER TABLE "RestaurantRecords" RENAME COLUMN temp_borough TO borough;

 -- Update ArrestRecords to conform to borough relation borough setup
UPDATE "ArrestRecords" SET arrest_borough = 'MN' WHERE TRIM("arrest_borough") = 'M';
UPDATE "ArrestRecords" SET arrest_borough = 'BK' WHERE TRIM("arrest_borough") = 'K';
UPDATE "ArrestRecords" SET arrest_borough = 'QN' WHERE TRIM("arrest_borough") = 'Q';
UPDATE "ArrestRecords" SET arrest_borough = 'SI' WHERE TRIM("arrest_borough") = 'S';
UPDATE "ArrestRecords" SET arrest_borough = 'BX' WHERE TRIM("arrest_borough") = 'B';

ALTER TABLE "ArrestRecords" ADD COLUMN temp_borough borough_name;
UPDATE "ArrestRecords" SET temp_borough = "arrest_borough"::borough_name;
ALTER TABLE "ArrestRecords" DROP COLUMN "arrest_borough";
ALTER TABLE "ArrestRecords" RENAME COLUMN temp_borough TO arrest_borough;


-- Update CrashRecords to conform to borough relation
UPDATE "CrashRecords" SET crash_borough = 'MN' WHERE TRIM("crash_borough") = 'MANHATTAN';
UPDATE "CrashRecords" SET crash_borough = 'BK' WHERE TRIM("crash_borough") = 'BROOKLYN';
UPDATE "CrashRecords" SET crash_borough = 'QN' WHERE TRIM("crash_borough") = 'QUEENS';
UPDATE "CrashRecords" SET crash_borough = 'SI' WHERE TRIM("crash_borough") = 'STATEN ISLAND';
UPDATE "CrashRecords" SET crash_borough = 'BX' WHERE TRIM("crash_borough") = 'BRONX';

-- Update PropertyRecords to conform to borough relation
UPDATE "PropertyRecords" SET property_borough = 'MN' WHERE TRIM("property_borough") = 'MANHATTAN';
UPDATE "PropertyRecords" SET property_borough = 'BK' WHERE TRIM("property_borough") = 'BROOKLYN';
UPDATE "PropertyRecords" SET property_borough = 'QN' WHERE TRIM("property_borough") = 'QUEENS';
UPDATE "PropertyRecords" SET property_borough = 'SI' WHERE TRIM("property_borough") = 'STATEN IS';
UPDATE "PropertyRecords" SET property_borough = 'BX' WHERE TRIM("property_borough") = 'BRONX';

ALTER TABLE "PropertyRecords" ADD COLUMN temp_borough borough_name;
UPDATE "PropertyRecords" SET temp_borough = "property_borough"::borough_name;
ALTER TABLE "PropertyRecords" DROP COLUMN "property_borough";
ALTER TABLE "PropertyRecords" RENAME COLUMN temp_borough TO property_borough;



-- some borough information is NULL on crash record, use long and lat to find correct borough
UPDATE "CrashRecords"
SET crash_borough = subquery.borough
FROM (
    SELECT "CrashRecords".collision_id, "TaxiZones".borough
    FROM "CrashRecords", "TaxiZones"
    WHERE ST_Contains("TaxiZones".geom, ST_SetSRID(ST_Point("CrashRecords".longitude, "CrashRecords".latitude), 4326))
    AND "CrashRecords".crash_borough IS NULL
) AS subquery
WHERE "CrashRecords".collision_id = subquery.collision_id;

-- NOTE SOME CRASH INFO BOROUGHS REMAIN NULL BECAUSE THEY OCCUR IN A LOCATION THAT
-- IS NOT PART OF A BOROUGH, I.E A BRIDGE CONNECTING BOROUGHS
-- EXAMPLE BELOW OCCURS ON VERRAZZANO NARROWS BRIDGE
-- COULD USE THIS TO WRITE A QUERY, HOW MANY ACCIDENTS OCCUR ON BRIDGES?

-- TO HANDLE THIS, CREATE SUBTABLE CALLED BOROUGH CRASHES THAT CONTAINS ALL CRASHES THAT OCCURED IN BOROUGHS
CREATE TABLE "BoroughCrashes" AS TABLE "CrashRecords";
DELETE FROM "BoroughCrashes"
WHERE crash_borough IS NULL;

ALTER TABLE "BoroughCrashes" ADD COLUMN temp_borough borough_name;
UPDATE "BoroughCrashes" SET temp_borough = "crash_borough"::borough_name;
ALTER TABLE "BoroughCrashes" DROP COLUMN "crash_borough";
ALTER TABLE "BoroughCrashes" RENAME COLUMN temp_borough TO crash_borough;

-- NOW DROP crash_borough from CrashRecords to normalize
ALTER TABLE "CrashRecords"
DROP COLUMN crash_borough;




