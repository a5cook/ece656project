
-- crash record trigger that checks injuries & fatalities sum
CREATE OR REPLACE FUNCTION fn_check_crash_record_totals()
RETURNS TRIGGER AS $$
BEGIN
    -- Check for injuries
    IF NEW.number_of_persons_injured != NEW.number_of_pedestrians_injured + NEW.number_of_cyclist_injured + NEW.number_of_motorist_injured THEN
        RAISE EXCEPTION 'The total number of persons injured does not match the sum of detailed injuries.';
    END IF;

    -- Check for fatalities
    IF NEW.number_of_persons_killed != NEW.number_of_pedestrians_killed + NEW.number_of_cyclist_killed + NEW.number_of_motorist_killed THEN
        RAISE EXCEPTION 'The total number of persons killed does not match the sum of detailed fatalities.';
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_check_crash_record_totals
BEFORE INSERT OR UPDATE ON "CrashRecords"
FOR EACH ROW EXECUTE FUNCTION fn_check_crash_record_totals();





