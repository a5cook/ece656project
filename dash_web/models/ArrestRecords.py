from datetime import date

from sqlalchemy import (
    BigInteger,
    Double,
    Date,
    Integer,
    SmallInteger,
    Text,
    CHAR,
    ForeignKey,
)
from sqlalchemy.orm import declarative_base, Mapped, mapped_column, Relationship

from .Borough import Borough
from .CustomType import BoroughNameType, BoroughName, AgeGroupType, AgeGroup
from .TaxiZones import TaxiZones

Base = declarative_base()


class ArrestRecords(Base):
    __tablename__ = "ArrestRecords"
    arrest_key: Mapped[int] = mapped_column(
        BigInteger, primary_key=True, autoincrement=False
    )
    arrest_date: Mapped[date] = mapped_column(Date)
    pd_cd: Mapped[int] = mapped_column(SmallInteger)
    pd_desc: Mapped[str] = mapped_column(Text)
    ky_cd: Mapped[int] = mapped_column(SmallInteger)
    ofns_desc: Mapped[str] = mapped_column(Text)
    law_code: Mapped[str] = mapped_column(Text)
    law_cat_cd: Mapped[str] = mapped_column(Text)
    arrest_precinct: Mapped[int] = mapped_column(SmallInteger)
    jurisdiction_code: Mapped[int] = mapped_column(SmallInteger)
    age_group: Mapped[AgeGroup] = mapped_column(AgeGroupType)
    perp_sex: Mapped[str] = mapped_column(CHAR)
    perp_race: Mapped[str] = mapped_column(CHAR)
    x_coord: Mapped[int] = mapped_column(Integer)
    y_coord: Mapped[int] = mapped_column(Integer)
    latitude: Mapped[float] = mapped_column(Double)
    longitude: Mapped[float] = mapped_column(Double)
    arrest_borough: Mapped[BoroughName] = mapped_column(
        BoroughNameType, ForeignKey(Borough.name)
    )
    borough_item: Mapped[Borough] = Relationship(Borough)
    arrest_zone: Mapped[int] = mapped_column(Integer, ForeignKey(TaxiZones.gid))
    zone_item: Mapped[TaxiZones] = Relationship(TaxiZones)


if __name__ == "__main__":
    from pathlib import Path
    from sqlalchemy import create_engine, MetaData
    import tomli

    with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
        setting = tomli.load(f)

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    ArrestRecords.__table__.create(bind=engine)
