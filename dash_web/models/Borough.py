from sqlalchemy.orm import declarative_base, Mapped, mapped_column

from .CustomType import BoroughNameType, BoroughName

Base = declarative_base()


class Borough(Base):
    __tablename__ = "Borough"
    name: Mapped[BoroughName] = mapped_column(BoroughNameType, primary_key=True)


if __name__ == "__main__":
    from pathlib import Path
    from sqlalchemy import create_engine
    import tomli

    with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
        setting = tomli.load(f)

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    Borough.__table__.create(bind=engine)
