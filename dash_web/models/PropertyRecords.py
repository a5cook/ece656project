from sqlalchemy import Text, BigInteger, Double, ForeignKey, Integer
from sqlalchemy.orm import declarative_base, Mapped, mapped_column, Relationship

from .Borough import Borough
from .CustomType import BoroughNameType, BoroughName
from .TaxiZones import TaxiZones

Base = declarative_base()


class PropertyRecords(Base):
    __tablename__ = "PropertyRecords"

    bble: Mapped[str] = mapped_column(Text, primary_key=True)
    block: Mapped[int] = mapped_column(BigInteger)
    lot: Mapped[int] = mapped_column(BigInteger)
    easement: Mapped[str] = mapped_column(Text)
    owner: Mapped[str] = mapped_column(Text)
    bldgcl: Mapped[str] = mapped_column(Text)
    taxclass: Mapped[str] = mapped_column(Text)
    ltfront: Mapped[int] = mapped_column(BigInteger)
    ltdepth: Mapped[int] = mapped_column(BigInteger)
    ext: Mapped[str] = mapped_column(Text)
    stories: Mapped[float] = mapped_column(Double)
    fullval: Mapped[int] = mapped_column(BigInteger)
    avland: Mapped[int] = mapped_column(BigInteger)
    avtot: Mapped[int] = mapped_column(BigInteger)
    exland: Mapped[int] = mapped_column(BigInteger)
    extot: Mapped[int] = mapped_column(BigInteger)
    excd1: Mapped[float] = mapped_column(Double)
    staddr: Mapped[str] = mapped_column(Text)
    postcode: Mapped[float] = mapped_column(Double)
    exmptcl: Mapped[str] = mapped_column(Text)
    bldfront: Mapped[int] = mapped_column(BigInteger)
    blddepth: Mapped[int] = mapped_column(BigInteger)
    year: Mapped[str] = mapped_column(Text, primary_key=True)
    valtype: Mapped[str] = mapped_column(Text)
    latitude: Mapped[float] = mapped_column(Double)
    longitude: Mapped[float] = mapped_column(Double)
    community_board: Mapped[float] = mapped_column(Double)
    council_district: Mapped[float] = mapped_column(Double)
    census_tract: Mapped[float] = mapped_column(Double)
    bin: Mapped[float] = mapped_column(Double)
    nta: Mapped[str] = mapped_column(Text)
    property_borough: Mapped[BoroughName] = mapped_column(
        BoroughNameType, ForeignKey(Borough.name)
    )
    borough_item: Mapped[Borough] = Relationship(Borough)
    property_zone: Mapped[int] = mapped_column(Integer, ForeignKey(TaxiZones.gid))
    zone_item: Mapped[TaxiZones] = Relationship(TaxiZones)


if __name__ == "__main__":
    from pathlib import Path
    from sqlalchemy import create_engine
    import tomli

    with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
        setting = tomli.load(f)

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    PropertyRecords.__table__.create(bind=engine)
