from sqlalchemy import Integer, ForeignKey, Numeric, VARCHAR
from sqlalchemy.orm import declarative_base, Mapped, mapped_column, Relationship
from geoalchemy2 import Geometry, WKTElement

from .Borough import Borough
from .CustomType import BoroughName, BoroughNameType

Base = declarative_base()


class TaxiZones(Base):
    __tablename__ = "TaxiZones"

    gid: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=False)
    shape_leng: Mapped[int] = mapped_column(Numeric)
    shape_area: Mapped[int] = mapped_column(Numeric)
    zone: Mapped[str] = mapped_column(VARCHAR)
    geom: Mapped[WKTElement] = mapped_column(
        Geometry(geometry_type="MULTIPOLYGON", srid=4326)
    )
    borough: Mapped[BoroughName] = mapped_column(
        BoroughNameType, ForeignKey(Borough.name)
    )
    borough_item: Mapped[Borough] = Relationship(Borough)


if __name__ == "__main__":
    from pathlib import Path
    from sqlalchemy import create_engine
    import tomli

    with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
        setting = tomli.load(f)

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    TaxiZones.__table__.create(bind=engine)
