from datetime import datetime

from sqlalchemy import Integer, BigInteger, Text, Double, DateTime, ForeignKey
from sqlalchemy.orm import declarative_base, Mapped, mapped_column, Relationship

from .TaxiZones import TaxiZones

Base = declarative_base()


class TaxiRecords(Base):
    __tablename__ = "TaxiRecords"

    TripId = mapped_column(BigInteger, primary_key=True, autoincrement=False)
    tpep_pickup_datetime: Mapped[datetime] = mapped_column(DateTime(timezone=False))
    tpep_dropoff_datetime: Mapped[datetime] = mapped_column(DateTime(timezone=False))
    passenger_count: Mapped[float] = mapped_column(Double)
    trip_distance: Mapped[float] = mapped_column(Double)
    RatecodeID: Mapped[int] = mapped_column(Integer)
    store_and_fwd_flag: Mapped[str] = mapped_column(Text)
    PULocationID: Mapped[int] = mapped_column(Integer, ForeignKey(TaxiZones.gid))
    DOLocationID: Mapped[int] = mapped_column(Integer)
    payment_type: Mapped[int] = mapped_column(BigInteger)
    fare_amount: Mapped[float] = mapped_column(Double)
    extra: Mapped[float] = mapped_column(Double)
    mta_tax: Mapped[float] = mapped_column(Double)
    tip_amount: Mapped[float] = mapped_column(Double)
    tolls_amount: Mapped[float] = mapped_column(Double)
    improvement_surcharge: Mapped[float] = mapped_column(Double)
    total_amount: Mapped[float] = mapped_column(Double)
    congestion_surcharge: Mapped[float] = mapped_column(Double)
    Airport_fee: Mapped[float] = mapped_column(Double)
    zone_item: Mapped[TaxiZones] = Relationship(TaxiZones)


if __name__ == "__main__":
    from pathlib import Path
    from sqlalchemy import create_engine
    import tomli

    with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
        setting = tomli.load(f)

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    TaxiRecords.__table__.create(bind=engine)
