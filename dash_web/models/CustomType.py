from enum import StrEnum

from sqlalchemy.dialects.postgresql import ENUM


class BoroughName(StrEnum):
    MN = "MN"
    BK = "BK"
    QN = "QN"
    BX = "BX"
    SI = "SI"
    EWR = "EWR"


BoroughNameType = ENUM(BoroughName, name="borough_name", create_type=False)


class AgeGroup(StrEnum):
    group_0 = "<18"
    group_1 = "18-24"
    group_2 = "25-44"
    group_3 = "45-64"
    group_4 = "65+"


AgeGroupType = ENUM(*[e.value for e in AgeGroup], name="age_group_enum", create_type=False)