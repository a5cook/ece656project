from sqlalchemy import Date, Text, Double, Integer, BigInteger, ForeignKey
from sqlalchemy.orm import declarative_base, Mapped, mapped_column, Relationship

from .Borough import Borough
from .CustomType import BoroughName, BoroughNameType
from .TaxiZones import TaxiZones

Base = declarative_base()


class RestaurantRecords(Base):
    __tablename__ = "RestaurantRecords"

    restaurant_name: Mapped[str] = mapped_column(Text, primary_key=True)
    seating_choice: Mapped[str] = mapped_column(Text)
    address: Mapped[str] = mapped_column(Text, primary_key=True)
    restaurant_inspection_id: Mapped[int] = mapped_column(BigInteger)
    inspected_on: Mapped[Date] = mapped_column(Date)
    zipcode: Mapped[int] = mapped_column(Integer)
    latitude: Mapped[float] = mapped_column(Double)
    longitude: Mapped[float] = mapped_column(Double)
    community_board: Mapped[int] = mapped_column(Integer)
    council_district: Mapped[int] = mapped_column(Integer)
    census_tract: Mapped[int] = mapped_column(Integer)
    bin: Mapped[int] = mapped_column(Integer)
    bbl: Mapped[int] = mapped_column(Integer)
    restaurant_borough: Mapped[BoroughName] = mapped_column(
        BoroughNameType, ForeignKey(Borough.name)
    )
    borough_item: Mapped[Borough] = Relationship(Borough)
    restaurant_zone: Mapped[int] = mapped_column(Integer, ForeignKey(TaxiZones.gid))
    zone_item: Mapped[TaxiZones] = Relationship(TaxiZones)


if __name__ == "__main__":
    from pathlib import Path
    from sqlalchemy import create_engine
    import tomli

    with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
        setting = tomli.load(f)

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    RestaurantRecords.__table__.create(bind=engine)
