from datetime import date, datetime

from sqlalchemy import Date, DateTime, Text, Double, Integer, BigInteger
from sqlalchemy.orm import declarative_base, Mapped, mapped_column

Base = declarative_base()


class CrashRecords(Base):
    __tablename__ = "CrashRecords"

    crash_date: Mapped[date] = mapped_column(Date)
    crash_time: Mapped[datetime] = mapped_column(DateTime(timezone=False))
    zipcode: Mapped[str] = mapped_column(Text)
    latitude: Mapped[float] = mapped_column(Double)
    longitude: Mapped[float] = mapped_column(Double)
    on_street_name: Mapped[str] = mapped_column(Text)
    cross_street_name: Mapped[str] = mapped_column(Text)
    off_street_name: Mapped[str] = mapped_column(Text)
    number_of_persons_injured: Mapped[int] = mapped_column(Integer)
    number_of_persons_killed: Mapped[int] = mapped_column(Integer)
    number_of_pedestrians_injured: Mapped[int] = mapped_column(Integer)
    number_of_pedestrians_killed: Mapped[int] = mapped_column(Integer)
    number_of_cyclist_injured: Mapped[int] = mapped_column(Integer)
    number_of_cyclist_killed: Mapped[int] = mapped_column(Integer)
    number_of_motorist_injured: Mapped[int] = mapped_column(Integer)
    number_of_motorist_killed: Mapped[int] = mapped_column(Integer)
    contributing_factor_vehicle_1: Mapped[str] = mapped_column(Text)
    contributing_factor_vehicle_2: Mapped[str] = mapped_column(Text)
    contributing_factor_vehicle_3: Mapped[str] = mapped_column(Text)
    contributing_factor_vehicle_4: Mapped[str] = mapped_column(Text)
    contributing_factor_vehicle_5: Mapped[str] = mapped_column(Text)
    collision_id: Mapped[int] = mapped_column(
        BigInteger, primary_key=True, autoincrement=False
    )
    vehicle_type_code_1: Mapped[str] = mapped_column(Text)
    vehicle_type_code_2: Mapped[str] = mapped_column(Text)
    vehicle_type_code_3: Mapped[str] = mapped_column(Text)
    vehicle_type_code_4: Mapped[str] = mapped_column(Text)
    vehicle_type_code_5: Mapped[str] = mapped_column(Text)


if __name__ == "__main__":
    from pathlib import Path
    from sqlalchemy import create_engine
    import tomli

    with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
        setting = tomli.load(f)

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    CrashRecords.__table__.create(bind=engine)
