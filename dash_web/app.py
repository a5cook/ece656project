import dash
import dash_bootstrap_components as dbc
from dash import Dash, html

app = Dash(
    __name__,
    meta_tags=[{"name": "viewport", "content": "width=device-width"}],
    external_stylesheets=[dbc.themes.BOOTSTRAP],
    use_pages=True,
    suppress_callback_exceptions=True,
)

app.title = "New York City Records"

app.layout = html.Div(
    [
        dbc.NavbarSimple(
            children=[
                dbc.NavItem(dbc.NavLink(page["name"], href=page["relative_path"]))
                for page in dash.page_registry.values()
            ],
            brand="New York City DataBase",
            brand_href="/",
            color="primary",
            dark=True,
            fluid=True,
        ),
        dash.page_container,
    ]
)

if __name__ == "__main__":
    app.run(debug=True)
