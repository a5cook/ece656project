from datetime import date
from pathlib import Path

import pandas as pd
import tomli
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from dash_web.models import BoroughCrashes, TaxiRecords

if __name__ == "__main__":
    with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
        setting = tomli.load(f)

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    with Session(engine) as session:
        query = (
            session.query(
                BoroughCrashes.collision_id,
                BoroughCrashes.crash_date,
                BoroughCrashes.crash_time,
                BoroughCrashes.borough_crash_zone,
                BoroughCrashes.number_of_persons_injured,
                BoroughCrashes.number_of_persons_killed,
                BoroughCrashes.crash_borough,
            )
            .filter(BoroughCrashes.crash_date >= date(2024, 1, 1))
            .order_by(BoroughCrashes.crash_date.desc())
        )
        df = pd.read_sql(query.statement, query.session.bind)
        df.to_csv(
            "BoroughCrashes.csv",
            index=False,
        )

        query = (
            session.query(
                TaxiRecords.TripId,
                TaxiRecords.tpep_pickup_datetime,
                TaxiRecords.tpep_dropoff_datetime,
                TaxiRecords.passenger_count,
                TaxiRecords.PULocationID,
                TaxiRecords.DOLocationID,
                TaxiRecords.fare_amount,
            )
            .filter(TaxiRecords.tpep_pickup_datetime > date(2023, 12, 31))
            .order_by(TaxiRecords.tpep_pickup_datetime.desc())
        )
        df = pd.read_sql(query.statement, query.session.bind)
        df.to_csv(
            "TaxiRecords.csv",
            index=False,
        )
