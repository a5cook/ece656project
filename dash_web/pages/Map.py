import json
from datetime import datetime as dt
from pathlib import Path

import dash
import dash_bootstrap_components as dbc
import pandas as pd
import plotly.express as px
import tomli
from dash import html, callback, Input, Output, dcc, State
from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.orm import Session

from dash_web.models import (
    ArrestRecords,
    BoroughCrashes,
    BusinessRecords,
    PropertyRecords,
    RestaurantRecords,
    TaxiRecords,
    TaxiZones,
)

with open(
    Path(__file__).parent.parent / "taxi_zones" / "zones.geojson", "r"
) as geofile:
    j_file = json.load(geofile)

with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
    setting = tomli.load(f)

dash.register_page(__name__)

table_options = [
    {"label": "Arrest Records", "value": "ArrestRecords"},
    {"label": "Borough Crashes", "value": "BoroughCrashes"},
    {"label": "Business Records", "value": "BusinessRecords"},
    {"label": "Property Records", "value": "PropertyRecords"},
    {"label": "RestaurantRecords", "value": "Restaurant Records"},
    {"label": "Taxi Records", "value": "TaxiRecords"},
]

table_name = {
    "ArrestRecords": ArrestRecords,
    "BoroughCrashes": BoroughCrashes,
    "BusinessRecords": BusinessRecords,
    "PropertyRecords": PropertyRecords,
    "RestaurantRecords": RestaurantRecords,
    "TaxiRecords": TaxiRecords,
}

time_features = {
    "ArrestRecords": ArrestRecords.arrest_date,
    "BoroughCrashes": BoroughCrashes.crash_date,
    "BusinessRecords": None,
    "PropertyRecords": None,
    "RestaurantRecords": None,
    "TaxiRecords": TaxiRecords.tpep_pickup_datetime,
}

primary_keys = {
    "ArrestRecords": ArrestRecords.arrest_key,
    "BoroughCrashes": BoroughCrashes.collision_id,
    "BusinessRecords": BusinessRecords.company_name,
    "PropertyRecords": PropertyRecords.bble,
    "RestaurantRecords": RestaurantRecords.restaurant_name,
    "TaxiRecords": TaxiRecords.TripId,
}

zone_features = {
    "ArrestRecords": ArrestRecords.arrest_zone,
    "BoroughCrashes": BoroughCrashes.borough_crash_zone,
    "BusinessRecords": BusinessRecords.business_zone,
    "PropertyRecords": PropertyRecords.property_zone,
    "RestaurantRecords": RestaurantRecords.restaurant_zone,
    "TaxiRecords": TaxiRecords.DOLocationID,
}

layout = dbc.Container(
    dbc.Row(
        children=[
            dbc.Col(
                children=[
                    html.H2("Map View"),
                    html.P("""Select different days using the date picker"""),
                    dcc.DatePickerRange(
                        id="date-picker",
                        min_date_allowed=dt(2023, 1, 1),
                        max_date_allowed=dt.today(),
                        initial_visible_month=dt(2023, 1, 1),
                        start_date=dt(2023, 1, 1).date(),
                        end_date=dt.today(),
                        style={"width": "100%"},
                    ),
                    dcc.Dropdown(
                        id="insight-table-dropdown",
                        options=table_options,
                        placeholder="Select Table",
                        value="ArrestRecords",
                    ),
                    dbc.Button("Search", color="primary", id="map-search-button"),
                ],
                width=3,
            ),
            dbc.Col(
                children=[
                    dcc.Graph(
                        id="map-graph",
                    ),
                    # dcc.Graph(id="histogram"),
                ],
                width=9,
            ),
        ],
    ),
    fluid=True,
)


@callback(
    Output("map-graph", "figure"),
    Input("map-search-button", "n_clicks"),
    [
        State("date-picker", "start_date"),
        State("date-picker", "end_date"),
        State("insight-table-dropdown", "value"),
    ],
    prevent_initial_call=True,
)
def insight(n_clicks, start_date, end_date, table):
    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    if not len(table) or table not in table_name:
        return dash.no_update

    if end_date < start_date:
        return dash.no_update

    time_feature = time_features[table]

    with Session(engine) as session:
        query = session.query(
            func.count(primary_keys[table]).label("total_count"),
            zone_features[table].label("gid"),
            TaxiZones.zone.label("zone_name"),
        ).join(TaxiZones)

        if time_feature:
            query = query.filter(time_feature >= start_date).filter(
                time_feature <= end_date
            )

        query = query.group_by(zone_features[table], TaxiZones.zone)
        df = pd.read_sql(query.statement, query.session.bind)

    fig = px.choropleth_mapbox(
        df,
        geojson=j_file,
        color="total_count",
        locations="gid",
        featureidkey="properties.LocationID",
        mapbox_style="carto-positron",
        zoom=9,
        center={"lat": 40.730610, "lon": -73.935242},
        opacity=0.5,
        labels={"total_count": "Number of records", "zone_name": "Zone Name"},
        hover_data={"gid": False, "total_count": True, "zone_name": True},
    )
    fig.update_geos(fitbounds="locations", visible=False)
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
    return fig
