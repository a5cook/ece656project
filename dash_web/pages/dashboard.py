from pathlib import Path

import dash
import dash_bootstrap_components as dbc
import tomli
from dash import get_asset_url, html
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from dash_web.models import (
    ArrestRecords,
    Borough,
    BoroughCrashes,
    BusinessRecords,
    CrashRecords,
    PropertyRecords,
    RestaurantRecords,
    TaxiRecords,
    TaxiZones,
)

with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
    setting = tomli.load(f)

dash.register_page(__name__, path="/")


def Cards(datas):
    card_list = []
    for data in datas:
        card = dbc.Card(
            [
                dbc.CardImg(src=get_asset_url(data["image"]), top=True),
                dbc.CardBody(
                    [
                        html.H4(data["title"], className="card-title"),
                        html.P(data["content"], className="card-text"),
                    ],
                ),
            ],
            style={"width": "18rem"},
        )
        col = dbc.Col(card)
        card_list.append(col)
    card_row = dbc.Container(
        dbc.Row(
            card_list,
            justify="center",
        ),
        fluid=True,
    )
    return card_row


def layout():
    all_table = {
        "Arrest Records": ArrestRecords,
        "Borough": Borough,
        "Borough Crashes": BoroughCrashes,
        "Business Records": BusinessRecords,
        "Crash Records": CrashRecords,
        "Property Records": PropertyRecords,
        "Restaurant Records": RestaurantRecords,
        "Taxi Records": TaxiRecords,
        "Taxi Zones": TaxiZones,
    }

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    datas = []
    with Session(engine) as session:
        for table in all_table:
            image_filename = f"{table.lower()}.jpg"
            print(image_filename)
            data = {"image": image_filename, "title": table}
            count = session.query(all_table[table]).count()
            data["content"] = "number of records: {}".format(count)
            datas.append(data)
        return Cards(datas)
