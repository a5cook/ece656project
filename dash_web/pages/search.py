from pathlib import Path

import dash
import dash_ag_grid as dag
import dash_bootstrap_components as dbc
import pandas as pd
import tomli
from dash import html, callback, Input, Output, dcc, State, ctx
from sqlalchemy import create_engine, text
from sqlalchemy.orm import Session

from dash_web.models import (
    ArrestRecords,
    Borough,
    BoroughCrashes,
    BusinessRecords,
    CrashRecords,
    PropertyRecords,
    RestaurantRecords,
    TaxiRecords,
    TaxiZones,
)

with open(Path(__file__).parent.parent / "setting.toml", "rb") as f:
    setting = tomli.load(f)

dash.register_page(__name__)

table_options = [
    {"label": "Arrest Records", "value": "ArrestRecords"},
    {"label": "Borough", "value": "Borough"},
    {"label": "Borough Crashes", "value": "BoroughCrashes"},
    {"label": "Business Records", "value": "BusinessRecords"},
    {"label": "Crash Records", "value": "CrashRecords"},
    {"label": "Property Records", "value": "PropertyRecords"},
    {"label": "RestaurantRecords", "value": "Restaurant Records"},
    {"label": "Taxi Records", "value": "TaxiRecords"},
    {"label": "Taxi Zones", "value": "TaxiZones"},
]

table_name = {
    "ArrestRecords": ArrestRecords,
    "Borough": Borough,
    "BoroughCrashes": BoroughCrashes,
    "BusinessRecords": BusinessRecords,
    "CrashRecords": CrashRecords,
    "PropertyRecords": PropertyRecords,
    "RestaurantRecords": RestaurantRecords,
    "TaxiRecords": TaxiRecords,
    "TaxiZones": TaxiZones,
}

layout = dbc.Container(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.H2("Search Data"),
                        dcc.Dropdown(
                            id="search-type-dropdown",
                            options=table_options,
                            placeholder="Select Table",
                            value="",
                        ),
                        html.Hr(),
                        dcc.Dropdown(
                            id="search-feature-dropdown",
                            options=[],
                            placeholder="Select a feature to search",
                            value="",
                        ),
                        dcc.Input(
                            id="search-input-value",
                            placeholder="Enter a value...",
                            type="text",
                            value="",
                            style={"width": "100%"},
                        ),
                        dcc.RadioItems(
                            id="search-operator-select",
                            options=[
                                {"label": "=", "value": "eq"},
                                {"label": ">", "value": "gt"},
                                {"label": "<", "value": "lt"},
                            ],
                            value="eq",
                            style={"width": "100%"},
                        ),
                        dbc.Button("Search", color="primary", id="search-button"),
                        html.Hr(),
                        dcc.Textarea(
                            id="advance-search-input",
                            placeholder="Advance Search",
                            style={"width": "100%"},
                        ),
                        dbc.Button(
                            "Search",
                            color="primary",
                            id="advance-search-button",
                        ),
                        html.Hr(),
                    ],
                    width=3,
                ),
                dbc.Col(
                    [
                        dag.AgGrid(
                            id="display-table",
                            columnSize="sizeToFit",
                            columnDefs=None,
                            rowData=None,
                            defaultColDef={"sortable": True},
                            # rowModelType="infinite",
                            # dashGridOptions={
                            #     # The number of rows rendered outside the viewable area the grid renders.
                            #     "rowBuffer": 0,
                            #     # How many blocks to keep in the store. Default is no limit, so every requested block is kept.
                            #     "maxBlocksInCache": 1,
                            #     "rowSelection": "multiple",
                            # },
                        ),
                    ],
                    id="table-col",
                    width=9,
                ),
            ],
        )
    ],
    fluid=True,
)


@callback(
    Output("search-feature-dropdown", "options"), Input("search-type-dropdown", "value")
)
def update_feature_from_type(value):
    if value not in table_name:
        return []
    return table_name[value].__table__.columns.keys()


@callback(
    [
        Output("display-table", "rowData"),
        Output("display-table", "columnDefs"),
    ],
    [Input("search-button", "n_clicks"), Input("advance-search-button", "n_clicks")],
    [
        State("search-type-dropdown", "value"),
        State("search-feature-dropdown", "value"),
        State("search-input-value", "value"),
        State("search-operator-select", "value"),
        State("advance-search-input", "value"),
    ],
    prevent_initial_call=True,
)
def standard_search(
    search_clicks, advanced_search_click, table, feature, value, operator, input_sql
):
    search_limit = 100

    db_setting = setting["database"]
    engine = create_engine(
        f"postgresql://{db_setting['user']}:{db_setting['password']}@{db_setting['host']}:{db_setting['port']}/{db_setting['database']}"
    )

    if ctx.triggered_id == "advance-search-button":
        if input_sql:
            with engine.connect() as connection:
                df = pd.read_sql(input_sql, connection)
            return df.to_dict("records"), [{"field": i} for i in df.columns]

    if not table or table not in table_name:
        return None, None
    if not feature or feature not in table_name[table].__table__.columns:
        return None, None

    if not len(value):
        with Session(engine) as session:
            query = session.query(table_name[table])
            query = query.limit(search_limit)
            df = pd.read_sql(query.statement, query.session.bind)
            print(query.statement)
        return df.to_dict("records"), [{"field": i} for i in df.columns]

    with Session(engine) as session:
        query = session.query(table_name[table])
        if operator == "eq":
            query = query.where(table_name[table].__table__.columns[feature] == value)
        if operator == "gt":
            query = query.where(table_name[table].__table__.columns[feature] > value)
        if operator == "lt":
            query = query.where(table_name[table].__table__.columns[feature] < value)
        query = query.limit(search_limit)
        df = pd.read_sql(query.statement, query.session.bind)
    # print(df)
    return df.to_dict("records"), [{"field": i} for i in df.columns]
