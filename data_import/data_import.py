import pandas as pd
from sqlalchemy import create_engine
import pyarrow.parquet as pq
import subprocess

pd.set_option('display.max_columns', None)

property_csv_file ='Property_Valuation_and_Assessment_Data_20240331.csv'
restaurant_csv_file = 'Open_Restaurants_Inspections_20240331.csv'
taxi_parquet_file = 'yellow_tripdata_2024-01.parquet'
collisions_csv_file = 'Motor_Vehicle_Collisions_-_Crashes_20240329.csv'
business_csv_file ='Mapped_In_NY_Companies_20240331.csv'
arrest_csv_file = 'NYPD_Arrest_Data__Year_to_Date__20240329.csv'


pyarrow_table = pq.read_table(taxi_parquet_file)
taxi_df = pyarrow_table.to_pandas()

property_df = pd.read_csv(property_csv_file)
restaurant_df = pd.read_csv(property_csv_file)
collisions_df = pd.read_csv(collisions_csv_file)
business_df = pd.read_csv(business_csv_file)
arrest_df = pd.read_csv(arrest_csv_file)

dbname = 'postgres'
user = 'postgres'
password = 'abc123'
host = 'localhost'
port = '5432'

property_table_name = 'PropertyRecords'
restaurant_table_name = 'RestaurantRecords'
taxi_table_name = 'TaxiRecords'
collision_table_name = 'CollisionRecords'
business_table_name = 'BusinessRecords'
arrest_table_name = 'ArresRecords'

conn_string = f'postgresql://{user}:{password}@{host}:{port}/{dbname}'
engine = create_engine(conn_string)

property_df.to_sql(property_table_name, engine, if_exists='replace', index=False)
restaurant_df.to_sql(restaurant_table_name, engine, if_exists='replace', index=False)
taxi_df.to_sql(taxi_table_name, engine, if_exists='replace', index=False)
collisions_df.to_sql(collision_table_name, engine, if_exists='replace', index=False)
business_df.to_sql(business_table_name, engine, if_exists='replace', index=False)
arrest_df.to_sql(arrest_table_name, engine, if_exists='replace', index=False)

print("Data has been successfully loaded into the database.")

# The following automates the shape file import
shp2pgsql_command = [
    'shp2pgsql', '-I', '-s', '2263',
    'taxi_zones/taxi_zones.shp'
]

psql_command = [
    'psql', '-U', 'postgres', '-d', 'postgres', '-h', 'localhost', '-p', '5432'
]

shp2pgsql_process = subprocess.Popen(
    shp2pgsql_command,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE
)

psql_process = subprocess.Popen(
    psql_command,
    stdin=shp2pgsql_process.stdout,
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE
)

shp2pgsql_stdout, shp2pgsql_stderr = shp2pgsql_process.communicate()

psql_stdout, psql_stderr = psql_process.communicate()

if shp2pgsql_process.returncode != 0:
    print("shp2pgsql encountered an error:")
    print(shp2pgsql_stderr.decode())

if psql_process.returncode != 0:
    print("psql encountered an error:")
    print(psql_stderr.decode())
else:
    print("Shapefile successfully imported into PostgreSQL database.")



