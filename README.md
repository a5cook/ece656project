# Project Overview

Presentation Link:
[https://youtu.be/s6HdCY4I73g](https://youtu.be/s6HdCY4I73g)

Report found under `/report.pdf`
Database setup scripts found under `database_setup/`
Client-side application found under `dash_web/`
Raw zipped data found under `data_import/`
Data analysis exercise found under `data_mining/`

# Project Setup

To set up this project, a local PostgreSQL server must be running on the target machine.

## Step 1
Spin up a local DB and unzip data to locate the shape file. Import the shape file into the DB using the following command:

`shp2pgsql -s <SRID> <shapefile> <table_name> <schema_name> > output.sql`

## Step 2

Import the remainder of the data using the script `data_import.py`, then execute the `.sql` files under `database_setup` in the following order:
`geo_setup.sql` > `data_cleanup.sql` > `create_borough.sql` > `key_setup.sql` > `triggers_setup.sql` 


## Step 3

Spin up the web application found under `dash_web`. To do so, set up a Python venv within the directory and execute the following:


`pipenv install`

This will ensure all dependencies are installed. After that, ensure `settings.toml` credentials match the database credentials from Step 1. The app can be launched from the `app.py` file.




